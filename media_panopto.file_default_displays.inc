<?php

/**
 * @file
 * Default display configuration for the default file types.
 */

/**
 * Implements hook_file_default_displays().
 */
function media_panopto_file_default_displays() {
  $file_displays = array();

  // Media: 7.x-2.x.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_panopto_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '640',
    'height' => '390',
    'autoplay' => FALSE,
    'autobuffer' => FALSE,
    'showbrand' => TRUE,
    'offerviewer' => TRUE,
    'start' => 0,
  );
  $file_displays['video__default__media_panopto_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__preview__media_panopto_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'media_thumbnail',
  );
  $file_displays['video__preview__media_panopto_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__teaser__media_panopto_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '560',
    'height' => '340',
    'autoplay' => FALSE,
    'autobuffer' => FALSE,
    'showbrand' => TRUE,
    'offerviewer' => TRUE,
    'start' => 0,
  );
  $file_displays['video__teaser__media_panopto_video'] = $file_display;

  // Media: 7.x-1.x.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__media_original__media_panopto_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '640',
    'height' => '390',
    'autoplay' => FALSE,
    'autobuffer' => FALSE,
    'showbrand' => TRUE,
    'offerviewer' => TRUE,
    'start' => 0,
  );
  $file_displays['video__media_original__media_panopto_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__media_large__media_panopto_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '640',
    'height' => '390',
    'autoplay' => FALSE,
    'autobuffer' => FALSE,
    'showbrand' => TRUE,
    'offerviewer' => TRUE,
    'start' => 0,
  );
  $file_displays['video__media_large__media_panopto_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__media_small__media_panopto_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '560',
    'height' => '340',
    'autoplay' => FALSE,
    'autobuffer' => FALSE,
    'showbrand' => TRUE,
    'offerviewer' => TRUE,
    'start' => 0,
  );
  $file_displays['video__media_small__media_panopto_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__media_preview__media_panopto_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'square_thumbnail',
  );
  $file_displays['video__media_preview__media_panopto_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__media_link__file_field_file_default';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $file_displays['video__media_link__file_field_file_default'] = $file_display;

  return $file_displays;
}
