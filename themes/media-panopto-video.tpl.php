<?php

/**
 * @file
 * Template file for displaying video.
 */
?>
<div class="<?php print $classes; ?> media-panopto-<?php print $id; ?>">
  <iframe class="media-panopto-player" width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="0" allowfullscreen><?php print $alternative_content; ?></iframe>
</div>
