<?php

/**
 * @file
 * Theme and preprocess functions.
 */

/**
 * Preprocess function for theme('media_panopto_video').
 */
function media_panopto_preprocess_media_panopto_video(&$variables) {
  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = check_plain($parts['id']);
  $variables['c'] = check_plain($parts['c']);

  // Make the file object available.
  $file_object = file_uri_to_object($variables['uri']);

  $query = array();
  $query['v'] = '1';

  // These queries default to false. If the option is true, set value to true.
  foreach (array('autoplay', 'autobuffer') as $option) {
    if ($variables['options'][$option]) {
      $query[$option] = 'true';
    }
    else {
      $query[$option] = 'false';
    }
  }
  // These queries default to 1. If the option is false, set value to 0.
  foreach (array('showbrand', 'offerviewer') as $option) {
    if (!$variables['options'][$option]) {
      $query[$option] = 'false';
    }
    else {
      $query[$option] = 'true';
    }
  }
  // Add some options as their own template variables.
  foreach (array('width', 'height') as $theme_var) {
    $variables[$theme_var] = $variables['options'][$theme_var];
  }

  // Do something useful with the overridden attributes from the file
  // object. We ignore alt and style for now.
  if (isset($variables['options']['attributes']['class'])) {
    if (is_array($variables['options']['attributes']['class'])) {
      $variables['classes_array'] = array_merge($variables['classes_array'], $variables['options']['attributes']['class']);
    }
    else {
      // Provide nominal support for Media 1.x.
      $variables['classes_array'][] = $variables['options']['attributes']['class'];
    }
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file_object->filename);
  // @TODO: Find an easy/ not too expensive way to get the Video description
  // to use for the alternative content.
  $variables['alternative_content'] = t('Video of @title', array('@title' => $variables['title']));

  // Build the iframe URL with options query string.
  $variables['url'] = url('//' . $variables['c'] . '/Panopto/Pages/Embed.aspx?id=' . $variables['video_id'], array('query' => $query, 'external' => TRUE));
}
