<?php

/**
 * @file
 * Extends the MediaReadOnlyStreamWrapper class to handle Panopto videos.
 */

/**
 * Stream wrapper class implementation for Panopto.
 */
class MediaPanoptoStreamWrapper extends MediaReadOnlyStreamWrapper {

  /**
   * Stream wrapper function which provides MIME type.
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'video/panopto';
  }

  /**
   * Stream wrapper function which provides thumbnail path.
   */
  public function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    return 'https://' . check_plain($parts['c']) . '/Panopto/PublicAPI/SessionPreviewImage?id=' . check_plain($parts['id']);
  }

  /**
   * Stream wrapper function which provides local path of thumbnail of video.
   */
  public function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    // There's no need to hide thumbnails, always use the public system rather
    // than file_default_scheme().
    $local_path = 'public://media-panopto/' . check_plain($parts['c']) . '_' . check_plain($parts['id']) . '.jpg';

    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());

      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }

    return $local_path;
  }

  /**
   * Stream wrapper function which provides actual URL of video.
   */
  public function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      return "https://" . $parameters['c'] . "/Panopto/Pages/Embed.aspx?id=" . check_plain($parameters['id']);
    }
  }

}
