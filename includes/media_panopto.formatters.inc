<?php

/**
 * @file
 * File formatters for Panopto videos.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_panopto_file_formatter_info() {
  $formatters['media_panopto_video'] = array(
    'label' => t('Panopto Video'),
    'file types' => array('video'),
    'default settings' => array(
      'width' => 640,
      'height' => 390,
      'autoplay' => FALSE,
      'autobuffer' => FALSE,
      'showbrand' => TRUE,
      'offerviewer' => TRUE,
      'start' => 0,
    ),
    'view callback' => 'media_panopto_file_formatter_video_view',
    'settings callback' => 'media_panopto_file_formatter_video_settings',
    'mime types' => array('video/panopto'),
  );

  $formatters['media_panopto_image'] = array(
    'label' => t('Panopto Preview Image'),
    'file types' => array('video'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_panopto_file_formatter_image_view',
    'settings callback' => 'media_panopto_file_formatter_image_settings',
    'mime types' => array('video/panopto'),
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_panopto_file_formatter_video_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  // WYSIWYG does not yet support video inside a running editor instance.
  if ($scheme == 'panopto' && empty($file->override['wysiwyg'])) {
    $element = array(
      '#theme' => 'media_panopto_video',
      '#uri' => $file->uri,
      '#options' => array(),
    );

    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = array();

    $attribs = [
      'width',
      'height',
      'autoplay',
      'autobuffer',
      'showbrand',
      'offerviewer',
      'start',
    ];
    foreach ($attribs as $setting) {
      if (isset($file->override[$setting])) {
        $file->override[$setting];
      }
      else {
        $element['#options'][$setting] = $display['settings'][$setting];
      }
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_panopto_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();

  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
  );
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
  );

  // @see https://support.panopto.com/articles/Documentation/embedding-session-mp4
  // Single Options.
  $element['autoplay'] = array(
    '#title' => t('The video will play as soon as the page is loaded.'),
    '#type' => 'checkbox',
    '#default_value' => $settings['autoplay'],
  );
  $element['autobuffer'] = array(
    '#title' => t('Causes the Embed to start buffering the video as soon as its loaded. This has no effect if autoplay is true.'),
    '#type' => 'checkbox',
    '#default_value' => $settings['autobuffer'],
  );

  $element['showbrand'] = array(
    '#title' => t('Do not show the custom logo on the splash screen. The accent color is used regardless of this setting. This has no effect if autoplay is true.'),
    '#type' => 'checkbox',
    '#default_value' => $settings['showbrand'],
  );
  $element['offerviewer'] = array(
    '#title' => t('Hide the top-right button that launches the Full Viewer.'),
    '#type' => 'checkbox',
    '#default_value' => $settings['offerviewer'],
  );
  $element['start'] = array(
    '#title' => t("Set the time to start the video at (in seconds). The default is 0. This works whether you're showing the splash screen or autoplaying."),
    '#type' => 'textfield',
    '#default_value' => $settings['start'],
  );

  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_panopto_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);
  if ($scheme == 'panopto') {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);

    if (empty($image_style) || !isset($valid_image_styles[$image_style])) {
      $element = array(
        '#theme' => 'image',
        '#path' => $wrapper->getOriginalThumbnailPath(),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_panopto_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();

  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );

  return $element;
}
