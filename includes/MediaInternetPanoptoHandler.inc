<?php

/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle Panopto videos.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaPanoptoMediaInternetPanoptoHandler extends MediaInternetBaseHandler {

  /**
   * Helper function used in claim method.
   */
  public function parse($embedCode) {
    // https://*.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=*
    $patterns = array(
      '@.*//(.*)/Panopto/Pages/Viewer\.aspx\?id\=([^"\&]+)@i',
      '@.*//(.*)/Panopto/Pages/Embed\.aspx\?id\=([^"\&]+)@i',
    );

    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      // @TODO: Parse is called often. Refactor so that valid ID is checked
      // when a video is added, but not every time the embedCode is parsed.
      if (isset($matches[1]) && isset($matches[2]) && self::validId($matches[1], $matches[2])) {
        return file_stream_wrapper_uri_normalize('panopto://c/' . $matches[1] . '/id/' . $matches[2]);
      }
    }
  }

  /**
   * Provides video details.
   *
   * @return array
   *   Provides vide details based on URL.
   */
  public function getHandlerDetails($embedCode) {
    // https://*.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=*
    $patterns = array(
      '@.*//(.*)/Panopto/Pages/Viewer\.aspx\?id\=([^"\&]+)@i',
      '@.*//(.*)/Panopto/Pages/Embed\.aspx\?id\=([^"\&]+)@i',
    );

    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      // @TODO: Parse is called often. Refactor so that valid ID is checked
      // when a video is added, but not every time the embedCode is parsed.
      if (isset($matches[1]) && isset($matches[2]) && self::validId($matches[1], $matches[2])) {
        return array('client' => $matches[1], 'id' => $matches[2]);
      }
    }
  }

  /**
   * Check if this class can handle the embed.
   *
   * @return bool
   *   True if it can manage the embed.
   */
  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  /**
   * Returns file object.
   *
   * @return object
   *   Returns the file object of embed code.
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    // Try to default the file name to the video's title.
    if (empty($file->fid) && $info = $this->getFileInfo()) {
      $file->filename = truncate_utf8($info['title'], 255);
    }

    return $file;
  }

  /**
   * Returns information about the media.
   *
   * @return array
   *   If information is available, an array containing 'title'.
   *   Otherwise, NULL.
   */
  public function getFileInfo() {
    $info = &drupal_static(__FUNCTION__);
    if (!isset($info)) {
      $info = NULL;
      // Implement SOAP API to get the title of the video.
      module_load_include('inc', 'media_panopto', 'includes/media_panopto.lib');
      $video_details = $this->getHandlerDetails($this->embedCode);
      try {
        $video_meta = media_panopto_get_meta_data($video_details['id'], $video_details['client']);
      }
      catch (Exception $e) {
        drupal_set_message($e->faultstring, 'error');
        $video_meta = FALSE;
      }
      if ($video_meta != FALSE) {
        $info['title'] = $video_meta->Name;
      }
      else {
        drupal_set_message(t('Unable to retrieve video meta data.'), 'error');
        $info['title'] = 'Unable to read meta data of video ' . uniqid();
      }
    }
    return $info;
  }

  /**
   * Check if a Panopto video ID is valid.
   *
   * @return bool
   *   TRUE if the video ID is valid, or throws a
   *   MediaInternetValidationException otherwise.
   */
  public static function validId($client, $video_id) {
    module_load_include('inc', 'media_panopto', 'includes/media_panopto.lib');
    try {
      media_panopto_get_meta_data($video_id, $client);
    }
    catch (Exception $e) {
      throw new MediaInternetValidationException($e->faultstring);
    }
    return TRUE;
  }

}
