<?php

/**
 * @file
 * Extends the SoapClient class to handle SOAP requests.
 */

/**
 * SOAP class implementation for Panopto.
 */
class MediaPanoptoPanoptoSessionManagementSoapClient extends SoapClient {
  /**
   * Namespace used for any root level objects.
   *
   * @var string
   */
  const ROOT_LEVEL_NAMESPACE = "http://tempuri.org/";

  /**
   * Namespace used for object members.
   *
   * @var string
   */
  const OBJECT_MEMBER_NAMESPACE = "http://schemas.datacontract.org/2004/07/Panopto.Server.Services.PublicAPI.V40";

  /**
   * Namespace used for sessionIds.
   *
   * @var string
   */
  const ARRAY_MEMBER_NAMESPACE = "http://schemas.microsoft.com/2003/10/Serialization/Arrays";

  /**
   * Namespace used for ArraysOfGuid.
   *
   * @var string
   */
  const SER_MEMBER_NAMESPACE = "http://schemas.microsoft.com/2003/10/Serialization/";

  /**
   * Username of calling user.
   *
   * @var string
   */
  public $ApiUserKey;

  /**
   * Auth code generated for calling user.
   *
   * @var string
   */
  public $ApiUserAuthCode;

  /**
   * Name of Panopto server being called.
   *
   * @var string
   */
  public $Servername;

  /**
   * Password needed if provider does not have a bounce page.
   *
   * @var string
   */
  public $Password;

  /**
   * Store the current action.
   *
   * @var string
   */
  public $currentaction;

  /**
   * Constructor function.
   */
  public function __construct($servername, $apiuseruserkey, $apiuserauthcode, $password) {
    $this->ApiUserKey = $apiuseruserkey;
    $this->ApiUserAuthCode = $apiuserauthcode;
    $this->Servername = $servername;
    $this->Password = $password;

    // Instantiate SoapClient in WSDL mode.
    // Set call timeout to 5 minutes.
    parent::__construct("https://" . $servername . "/Panopto/PublicAPI/4.6/SessionManagement.svc?wsdl");
  }

  /**
   * Helper method for making a call to the Panopto API.
   */
  public function callWebMethod($methodname, $namedparams = array(), $auth = TRUE) {
    $params = array();
    // Include API user and auth code params unless $auth is set to false.
    if ($auth) {
      // Create SoapVars for AuthenticationInfo object members.
      $authinfo = new stdClass();
      $authinfo->AuthCode = new SoapVar(
        $this->ApiUserAuthCode,
        XSD_STRING,
        NULL,
        NULL,
        NULL,
        self::OBJECT_MEMBER_NAMESPACE
      );

      // Add the password parameter if a password is provided.
      if (!empty($this->Password)) {
        $authinfo->Password = new SoapVar($this->Password, XSD_STRING, NULL, NULL, NULL, self::OBJECT_MEMBER_NAMESPACE);
      }
      $authinfo->AuthCode = new SoapVar($this->ApiUserAuthCode, XSD_STRING, NULL, NULL, NULL, self::OBJECT_MEMBER_NAMESPACE);
      $authinfo->UserKey = new SoapVar($this->ApiUserKey, XSD_STRING, NULL, NULL, NULL, self::OBJECT_MEMBER_NAMESPACE);

      // Create a container for storing all of the soap vars.
      $obj = array();
      // Add auth info to $obj container.
      $obj['auth'] = new SoapVar($authinfo, SOAP_ENC_OBJECT, NULL, NULL, NULL, self::ROOT_LEVEL_NAMESPACE);
      // Add the soapvars from namedparams to the container.
      foreach ($namedparams as $key => $value) {
        $obj[$key] = $value;
      }
      // Create a soap param using the obj container.
      $param = new SoapParam(new SoapVar($obj, SOAP_ENC_OBJECT), 'data');
      // Add the created soap param to an array to be passed to __soapCall.
      $params = array($param);
    }
    // Update current action with the method being called.
    $this->currentaction = "http://tempuri.org/ISessionManagement/$methodname";

    // Make the SOAP call via SoapClient::__soapCall.
    return parent::__soapCall($methodname, $params);
  }

  /**
   * Sample function for calling an API method.
   */
  public function getSessionList($request, $searchQuery) {
    $requestvar = new SoapVar($request, SOAP_ENC_OBJECT, NULL, NULL, NULL, self::ROOT_LEVEL_NAMESPACE);
    $searchQueryVar = new SoapVar($searchQuery, XSD_STRING, NULL, NULL, NULL, self::ROOT_LEVEL_NAMESPACE);
    return self::callWebMethod("GetSessionsList", array("request" => $requestvar, "searchQuery" => $searchQueryVar));
  }

  /**
   * Sample function for calling an API method.
   */
  public function getSessionsById($sessionId) {
    $searchQueryVar = new SoapVar($sessionId, SOAP_ENC_OBJECT, NULL, NULL, NULL, self::ROOT_LEVEL_NAMESPACE);
    return self::callWebMethod("GetSessionsById", array("sessionIds" => $searchQueryVar));
  }

  /**
   * Override SOAP action to work around bug in older PHP SOAP versions.
   */
  public function __doRequest($request, $location, $action, $version, $oneway = NULL) {
    return parent::__doRequest($request, $location, $this->currentaction, $version);
  }

}

/**
 * Function to get meta data of video from Panopto server.
 */
function media_panopto_get_meta_data($session_id, $client) {
  // The username of the calling panopto user.
  $user_key = token_replace(variable_get('media_panopto_sso_user_id_token', ''));

  // The name of the panopto server (i.e. demo.hosted.panopto.com).
  $server_name = $client;

  // The application key from provider on the Panopto provider's page.
  $application_key = variable_get('media_panopto_admin_key', '');

  // Password of the calling user on Panopto server.
  $password = NULL;

  // Generate an auth code.
  $auth_code = media_panopto_generate_auth_code($user_key, $server_name, $application_key);

  // Create a SOAP client for the desired Panopto API class.
  $session_management_client = new MediaPanoptoPanoptoSessionManagementSoapClient($server_name, $user_key, $auth_code, $password);

  // Set https endpoint in case wsdl specifies http.
  $session_management_client->__setLocation("https://" . $server_name . "/Panopto/PublicAPI/4.6/SessionManagement.svc");

  $session_id_obj = new SoapVar($session_id, XSD_STRING, NULL, NULL, NULL, MediaPanoptoPanoptoSessionManagementSoapClient::ARRAY_MEMBER_NAMESPACE);
  $array_of_guid = new stdClass();
  $array_of_guid->guid = $session_id_obj;
  $array_of_guid_obj = new SoapVar($array_of_guid, SOAP_ENC_OBJECT, NULL, NULL, NULL, MediaPanoptoPanoptoSessionManagementSoapClient::SER_MEMBER_NAMESPACE);

  // Call api and get respponse.
  $response_access_details = $session_management_client->getSessionsById($array_of_guid_obj);

  if (!empty($response_access_details->GetSessionsByIdResult->Session)) {
    return $response_access_details->GetSessionsByIdResult->Session;
  }
  else {
    return FALSE;
  }
}

/**
 * Function to create an API Authentication code.
 */
function media_panopto_generate_auth_code($userkey, $servername, $applicationkey) {
  $payload = $userkey . "@" . $servername;
  $signedpayload = $payload . "|" . $applicationkey;
  $authcode = strtoupper(sha1($signedpayload));
  return $authcode;
}
