CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage

INTRODUCTION
------------

Media: Panopto adds Panopto as a supported media provider.

REQUIREMENTS
------------

Media: Panopto has one dependency.

Contributed module
 * Media Internet - A submodule of the Media module.

INSTALLATION
------------

Media: Panopto can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).

CONFIGURATION
------------

Media: Panopto can be configured at admin/config/media/media_panopto
 * If your videos are public you can ignore these settings.
 * As Media module stores "Title" of video in Drupal this module needs a 
 way to fetch the meta data from Panopto.
 * If the video is not public the module cannot fetch the meta data.
 * To make it work you need to configure SSO here.
 * Set the admin key something like this 88267901-703a-7981-9456-875643b654d5.
 Get in touch with your Panopto point of contact to get this.
 * Set the Panopto SSO token. e.g. If you are using Canvas with Panopto 
 and CAS for Drupal authentication your SSO will look like this 
 "Canvas\[current-user:cas]"
 * Click on Gear icon which appears next to video thumbnail on 
 Panopto/Pages/Sessions/List.aspx page. In the overview tab you should see 
 the Owner value. Based on this value you can set the SSO token.
 * If you don't want everyone to update this setting you can give permission
 of "administer panopto configuration" to specific role.
 * You can also set & hide the admin key from everyone by adding following 
 setting in settings.php file
 * $conf['media_panopto_admin_key'] = '88267901-703a-7981-9456-875643b654d5';
 * $conf['media_panopto_admin_key_hide'] = TRUE;
 

USAGE
-----

Media: Panopto integrates the Panopto.com videos with the Media
module to allow users to add and manage Panopto videos.

Internet media can be added on the Web tab of the Add file page (file/add/web).
With this, users can add a Panopto video by entering its full or embed URL.
e.g. https://demo.hosted.panopto.com
/Panopto/Pages/Viewer.aspx?id=4cb263b2-9dea-461b-9f3b-ac7df4388a1d
